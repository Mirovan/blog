package ru.bigint.data.dao.blog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bigint.data.entity.blog.Category;
import ru.bigint.data.entity.blog.Post;

import java.util.List;

@Repository("postDAO")
public interface PostDAO extends JpaRepository<Post, Integer> {
    List<Post> findByCategoryInOrderByCreatedate(Category category);
}