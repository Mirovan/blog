package ru.bigint.data.dao.blog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bigint.data.entity.blog.Category;

@Repository("categoryDAO")
public interface CategoryDAO extends JpaRepository<Category, Integer> {
}