package ru.bigint.service.iface.blog;

import ru.bigint.data.entity.blog.Category;

import java.util.List;

public interface CategoryService {
    Category getById(Integer id);
    Category add(Category category);
    Category update(Category category);
    List<Category> getAll();
}
