package ru.bigint.service.iface.blog;

import ru.bigint.data.entity.blog.Category;
import ru.bigint.data.entity.blog.Post;

import java.util.List;

public interface PostService {
    Post getById(Integer id);
    Post add(Post post);
    Post update(Post post);
    List<Post> getByCategory(Category category);
    List<Post> getAll();
}
