package ru.bigint.service.iface.user;

import ru.bigint.data.entity.user.Role;

public interface RoleService {
    Role findRoleByCode(String code);
}
