package ru.bigint.service.impl.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.bigint.data.dao.user.RoleDAO;
import ru.bigint.data.entity.user.Role;
import ru.bigint.service.iface.user.RoleService;

@Service("roleService")
public class RoleServiceImpl implements RoleService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("roleDAO")
    private RoleDAO roleDAO;

    @Override
    public Role findRoleByCode(String code) {
        return roleDAO.findByCode(code);
    }
}
