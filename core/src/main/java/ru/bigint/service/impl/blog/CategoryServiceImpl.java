package ru.bigint.service.impl.blog;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.bigint.data.dao.blog.CategoryDAO;
import ru.bigint.data.entity.blog.Category;
import ru.bigint.service.iface.blog.CategoryService;

import java.util.List;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("categoryDAO")
    private CategoryDAO categoryDAO;

    @Override
    public Category getById(Integer id) {
        return categoryDAO.findById(id).get();
    }

    @Override
    public Category add(Category category) {
        category = categoryDAO.save(category);
        return category;
    }

    @Override
    public Category update(Category category) {
        category = categoryDAO.save(category);
        return category;
    }

    @Override
    public List<Category> getAll() {
        return categoryDAO.findAll();
    }
}