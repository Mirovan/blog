package ru.bigint.controller.cabinet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.bigint.data.entity.blog.Category;
import ru.bigint.data.entity.blog.Post;
import ru.bigint.service.iface.blog.CategoryService;
import ru.bigint.service.iface.blog.PostService;
import ru.bigint.utils.ImageUploader;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;


@Controller
public class CabinetPostController {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    Environment env;

    @Autowired
    private PostService postService;

    @Autowired
    private CategoryService categoryService;

    /**
     * Список Постов
     * */
    @RequestMapping(value="/cabinet/posts/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView showPosts() {
        List<Post> posts = postService.getAll();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("posts", posts);
        modelAndView.setViewName("cabinet/post/index");
        return modelAndView;
    }


    /**
     * Список Постов в категории
     * */
    @RequestMapping(value="/cabinet/category/{id}/posts/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView showPostsByCategory(@PathVariable Integer id) {
        Category category = categoryService.getById(id);
        List<Post> posts = postService.getByCategory(category);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("category", category);
        modelAndView.addObject("posts", posts);
        modelAndView.setViewName("cabinet/post/index");
        return modelAndView;
    }


    /**
     * Форма для Добавления поста
     * */
    @RequestMapping(value="/cabinet/post/add/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addPostForm() {
        Post post = new Post();
        List<Category> categories = categoryService.getAll();
        post.setCreatedate(new Timestamp(Calendar.getInstance().getTime().getTime()));

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("post", post);
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("action", "add");
        modelAndView.addObject("formAction", "/cabinet/post/add/");
        modelAndView.setViewName("cabinet/post/edit");
        return modelAndView;
    }


    /**
     * Добавление поста
     * */
    @RequestMapping(value="/cabinet/post/add/", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addPostSave(@ModelAttribute Post post, @RequestParam(value = "imageFile", required = false) MultipartFile imageFile) {
        String imageName = UUID.randomUUID().toString() + ".png";
        post.setImage(imageName);
        postService.add(post);

        //Сохраняем фото
        String imagePath = env.getProperty("blog.imagesUploadPath");
        ImageUploader.upload(imageFile, imagePath + "/" + imageName);

        return new ModelAndView("redirect:/cabinet/posts/");
    }


    /**
     * Форма для Редактирования поста
     * */
    @RequestMapping(value="/cabinet/post/{id}/edit/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView editPostForm(@PathVariable Integer id) {
        Post post = postService.getById(id);
        List<Category> categories = categoryService.getAll();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("post", post);
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("action", "edit");
        modelAndView.addObject("formAction", "/cabinet/post/edit/");
        modelAndView.setViewName("cabinet/post/edit");
        return modelAndView;
    }


    /**
     * Редактирование поста
     * */
    @RequestMapping(value="/cabinet/post/edit/", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView editPostSave(@ModelAttribute Post post, @RequestParam(value = "imageFile", required = false) MultipartFile imageFile) {
        String imageName = UUID.randomUUID().toString() + ".png";
        post.setImage(imageName);
        post.setCategory(categoryService.getById(post.getId()));
        postService.update(post);

        //Сохраняем фото
        String imagePath = env.getProperty("blog.imagesUploadPath");
        ImageUploader.upload(imageFile, imagePath + "/" + imageName);

        return new ModelAndView("redirect:/cabinet/posts/");
    }

}