package ru.bigint.controller.cabinet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.bigint.data.entity.blog.Category;
import ru.bigint.service.iface.blog.CategoryService;
import ru.bigint.service.iface.blog.PostService;

import java.util.List;


@Controller
public class CabinetCategoryController {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CategoryService categoryService;


    /**
     * Список категорий
     * */
    @RequestMapping(value="/cabinet/categories/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView showCategories() {
        ModelAndView modelAndView = new ModelAndView();
        List<Category> categories = categoryService.getAll();

        modelAndView.addObject("categories", categories);
        modelAndView.setViewName("cabinet/category/index");
        return modelAndView;
    }


    /**
     * Форма для Добавления категории
     * */
    @RequestMapping(value="/cabinet/category/add/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addCategoryForm() {
        Category category = new Category();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("category", category);
        modelAndView.addObject("action", "add");
        modelAndView.addObject("formAction", "/cabinet/category/add/");
        modelAndView.setViewName("cabinet/category/edit");
        return modelAndView;
    }


    /**
     * Добавление категории
     * */
    @RequestMapping(value="/cabinet/category/add/", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addCategorySave(@ModelAttribute Category category) {
        categoryService.add(category);

        return new ModelAndView("redirect:/cabinet/categories/");
    }


    /**
     * Форма для Редактирования Категории
     * */
    @RequestMapping(value="/cabinet/category/{id}/edit/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView editCategoryForm(@PathVariable Integer id) {
        Category category = this.categoryService.getById(id);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("category", category);
        modelAndView.addObject("action", "edit");
        modelAndView.addObject("formAction", "/cabinet/category/" + id + "edit/");
        modelAndView.setViewName("cabinet/category/edit");
        return modelAndView;
    }


    /**
     * Редактирование Категории
     * */
    @RequestMapping(value="/cabinet/category/edit/", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView editCategorySave(@ModelAttribute Category category) {
        this.categoryService.update(category);

        return new ModelAndView("redirect:/cabinet/categories/");
    }

}