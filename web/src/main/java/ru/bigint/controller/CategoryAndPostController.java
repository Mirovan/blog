package ru.bigint.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.bigint.data.entity.blog.Category;
import ru.bigint.data.entity.blog.Post;
import ru.bigint.service.iface.blog.CategoryService;
import ru.bigint.service.iface.blog.PostService;

import java.util.List;


@Controller
public class CategoryAndPostController {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PostService postService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    Environment env;

    /**
     * Список категорий
     * */
    @RequestMapping(value="/categories/", method = RequestMethod.GET)
    public ModelAndView showCategories() {
        ModelAndView modelAndView = new ModelAndView();
        List<Category> categories = categoryService.getAll();

        modelAndView.addObject("categories", categories);
        modelAndView.setViewName("category/list");
        return modelAndView;
    }


    /**
     * Список постов в категории
     * */
    @RequestMapping(value="/category/{id}/", method = RequestMethod.GET)
    public ModelAndView showPostsByCategories(@PathVariable Integer id) {
        Category category = categoryService.getById(id);
        List<Post> posts = postService.getByCategory(category);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("category", category);
        modelAndView.addObject("posts", posts);
        modelAndView.setViewName("post/list");
        return modelAndView;
    }


    /**
     * Пост
     * */
    @RequestMapping(value="/post/{id}/", method = RequestMethod.GET)
    public ModelAndView addLessonForm(@PathVariable Integer id) {
        Post post = postService.getById(id);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("post", post);
//        modelAndView.addObject("imagePath", env.getProperty("blog.imagesHandlerPath"));
        modelAndView.setViewName("post/index");
        return modelAndView;
    }
}